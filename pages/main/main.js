// pages/main/main.js
import { Main } from './main-model.js';

var main = new Main();

Page({

  data: {
    articleList: []
  },

  onLoad: function (options) {
    var _this = this;
    main.getAllArticle(function (res) {
      _this.setData({
        articleList: res
      });
    });
  },
  // 点击进入文章详情页
  onDetailsTap: function(event){
    var id = event.currentTarget.dataset.id;
    wx.navigateTo({
      url: './main-details/main-details?id=' + id
    })
  }
})