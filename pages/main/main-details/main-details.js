// pages/main/main-details/main-details.js
import { Article } from './main-details-model.js';

var article = new Article();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    article: {},
    comment: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    // 获取对应id的文章详情
    article.getArticleById(id, (res) => {
      console.log(res);
      this.setData({
        article: res.data
      });
    });
    // 获取对应id的评论
    article.getCommentById(id, (res) => {
      console.log(res);
      this.setData({
        comment: res.data
      });
    });
  },

  // 点击进入评论页
  onCommentTap: function(event){
    wx.navigateTo({
      url: '../../publish/publish-comment/publish-comment',
    })
  }
})