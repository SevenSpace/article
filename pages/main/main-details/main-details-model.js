import { Base } from '../../../utils/base.js';

class Article extends Base {
  constructor() {
    super();
  }

  getArticleById(id, callBack) {
    let params = {
      url: 'v1/article/getUserID',
      method: 'GET',
      data: { id },
      sCallBack: function (res) {
        callBack && callBack(res);
      }
    }

    this.request(params);
  }

  getCommentById(id, callBack) {
    let params = {
      url: 'v1/article/comment/getUserID',
      method: 'GET',
      data: { id },
      sCallBack: function (res) {
        callBack && callBack(res);
      }
    }

    this.request(params);
  }
  
}

export { Article };