import { Base } from '../../utils/base.js';

class Main extends Base {
  constructor() {
    super();
  }

  getAllArticle(callBack) {
    let params = {
      url: 'v1/article/getALL',
      method: 'GET',
      sCallBack: function (res) {
        console.log(res.data);
        callBack && callBack(res.data);
      }
    }

    this.request(params);
  }
}

export { Main };