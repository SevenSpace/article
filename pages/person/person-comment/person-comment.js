// pages/person/person-comment/person-comment.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  // 点击进入文章详情页
  onDetailsTap: function (event) {
    wx.navigateTo({
      url: '../../main/main-details/main-details'
    })
  }
})