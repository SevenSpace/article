// pages/person/person.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  // 点击进入我的文章页 
  onMyArticle: function(event){
    wx.navigateTo({
      url: './person-article/person-article',
    })
  },

  // 点击进入我的评论页
  onMyComment: function(event){
    wx.navigateTo({
      url: './person-comment/person-comment',
    })
  }
})