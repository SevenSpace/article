const host = 'http://localhost/test/public/api/';

class Base {
  constructor() {
    this.url = host;
  }

  request(params) {
    let url = this.url + params.url;
    if (!params.method) {
      params.method = 'GET';
    }
    wx.request({
      url: url,
      data: params.data,
      header: {
        'content-type': 'application/json'
      },
      method: params.method,
      success: function (res) {
        params.sCallBack && params.sCallBack(res.data);
      },
      fail: function (error) {
        console.log(error);
      },
    });
  }
}

export { Base };